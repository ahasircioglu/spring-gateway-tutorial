package gateway;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class TokenService {

    private final RestTemplate restTemplate;

    public String getToken() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("client_id", "admin-cli");
        map.add("username", "admin");
        map.add("password", "102030");
        map.add("grant_type", "password");
        map.add("client_secret", "5c426598-3549-4b54-97f1-2ddb08ddb5cd");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        System.out.println("getting token");

        ResponseEntity<TokenResponse> response = restTemplate.postForEntity("http://localhost:8080/auth/realms/master/protocol/openid-connect/token", request, TokenResponse.class);

        System.out.println(response);

        return response.getBody().accessToken;
    }

}
