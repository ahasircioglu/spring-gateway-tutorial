package gateway.configuration;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

@Configuration
public class HttpConfiguration {

    @Bean
    public DefaultResponseErrorHandler defaultResponseErrorHandler() {
        return new DefaultResponseErrorHandler();
    }

    @Bean
    public RestTemplate restTemplate(DefaultResponseErrorHandler defaultResponseErrorHandler) {
        return new RestTemplateBuilder()
                .errorHandler(defaultResponseErrorHandler)
                .build();
    }
}